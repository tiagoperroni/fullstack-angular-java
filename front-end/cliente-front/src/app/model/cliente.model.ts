export interface ClienteModel {
    id: string;
    nome: string;
    cpf: string;
    dataNascimento: string;
    dataCadastro?: string;
}