import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ClienteModel } from "src/app/model/cliente.model";
import { ClienteAppService } from "src/app/service/cliente-app.service";

@Component({
  selector: "app-listar-clientes",
  templateUrl: "./listar-clientes.component.html",
  styleUrls: ["./listar-clientes.component.css"],
})
export class ListarClientesComponent implements OnInit {
  clientes: ClienteModel[] = [];

  clienteBuilder: FormGroup;
  constructor(private service: ClienteAppService, private fb: FormBuilder, private activateRoute: ActivatedRoute) {
    this.clienteBuilder = this.fb.group({
      nome: [""],
      cpf: [""],
      dataNascimento: [""],
    });
    const  req = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getAll();
  }

  public getAll() {
    this.service.getAll().subscribe((res) => {
      console.log(res);
      
        return (this.clientes = res);
      }, (err) => {
        console.log(err);
      });
  }

  public create() {
    const cliente: ClienteModel = {  
      id: null!,    
      nome: this.clienteBuilder.get("nome")?.value,
      cpf: this.clienteBuilder.get("cpf")?.value,
      dataNascimento: this.clienteBuilder.get("dataNascimento")?.value,
    };
    return this.service.create(cliente).subscribe((res) => {
      this.clienteBuilder.reset();
     this.getAll();
      
    }, err => {
      console.log(err);      
    })
  }

  public delete(id: string) {
   return this.service.delete(id).subscribe((res) => {
    this.getAll();
    console.log('Produto deletado');
    
   }, err => {
     console.log(err);
     
   })
  }
}
