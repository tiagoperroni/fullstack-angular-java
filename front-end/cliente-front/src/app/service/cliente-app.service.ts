import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClienteModel } from '../model/cliente.model';

@Injectable({
  providedIn: 'root'
})
export class ClienteAppService {  

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getAll(): Observable<ClienteModel[]> {
    return this.http.get<ClienteModel[]>(this.apiUrl);
  } 

  public create(cliente: ClienteModel): Observable<ClienteModel> {
    return this.http.post<ClienteModel>(this.apiUrl, cliente);
  }

  public delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }
}
