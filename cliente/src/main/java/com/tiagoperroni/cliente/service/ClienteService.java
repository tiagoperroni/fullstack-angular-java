package com.tiagoperroni.cliente.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tiagoperroni.cliente.model.Cliente;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    List<Cliente> clientes = new ArrayList<>();

    Cliente cliente = new Cliente();

    public List<Cliente> obterTodos() {
        return clientes;
    }

    public Cliente buscarPorId(String id) {
        clientes.forEach(cl -> {
            if (cl.getId().equals(id)) {
                this.cliente = cl;
            }
        });

        return this.cliente;
    }

    public Cliente salvarCliente(Cliente cliente) {
        cliente.setId(UUID.randomUUID().toString());
        clientes.add(cliente);
        return cliente;
    }

    public String updateCliente(String id, Cliente cliente) {
        clientes.forEach(cl -> {
            if (cl.getId().equals(id)) {
                BeanUtils.copyProperties(cliente, cl, "id");                
            }
        });
        return "Cliente atualizado com sucesso.";
    }

    public void deleteCliente(String id) {
       try {
        clientes.forEach(cl -> {
            if (cl.getId().equals(id)) {
                clientes.remove(cl);
            }
        });  
       } catch (Exception e) {
           System.out.println(e.getMessage());
       } 
    }
}
