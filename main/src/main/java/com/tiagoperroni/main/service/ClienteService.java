package com.tiagoperroni.main.service;

import java.util.Date;
import java.util.UUID;

import com.tiagoperroni.main.model.Cliente;

import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    Cliente cliente = new Cliente(null, "Juares Soares", "054.569.874-89", "17/08/1977", new Date());

    public Cliente getCliente() {
        cliente.setId(UUID.randomUUID().toString());
        return this.cliente;
    }

}
